﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer.Models.Users;

namespace ExternalServiceLayer.Interfaces
{
    public interface IUser_Services
    {
        Task<List<User>> GetAllUSers();
        Task<User> GetUser(int userId);
    }
}
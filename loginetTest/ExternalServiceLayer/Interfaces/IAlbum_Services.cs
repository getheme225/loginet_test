﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer.Models.Album;

namespace ExternalServiceLayer.Interfaces
{
    public interface IAlbum_Services
    {
        Task<Album> GetAlbumByID(int Id);
        Task<ICollection<Album>> GetAllAlbum();
        Task<List<Album>>GetAlbumByUser(int userId);
    }
}
﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace ExternalServiceLayer.Utilit
{
    public static class QueryBuilder
    {
        public static string ToQueryString (NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            return "?" + string.Join("&", array);
        }
    }
}

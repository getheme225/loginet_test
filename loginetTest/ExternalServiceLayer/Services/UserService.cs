﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer.Models.Users;
using ExternalServiceLayer.Interfaces;



namespace ExternalServiceLayer.Services
{
    public class UserService:RequestBase<User>,IUser_Services
    {
        public UserService(string uri = "users") : base(uri){}

        public async Task<List<User>> GetAllUSers() => (List<User>) await GetAll();

        public async Task<User> GetUser(int userId) => await GetById(userId);

    }
}
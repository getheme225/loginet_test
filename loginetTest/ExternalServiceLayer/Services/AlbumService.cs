﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using DataLayer.Models.Album;
using ExternalServiceLayer.Interfaces;

namespace ExternalServiceLayer.Services
{
    public class AlbumService:RequestBase<Album>,IAlbum_Services
    {
        
        public AlbumService(string requesturi = "albums") : base(requesturi){}
      
        public  Task<Album> GetAlbumByID(int Id) =>  GetById(Id);
       
        public  Task<ICollection<Album>> GetAllAlbum() {
            return GetAll();
        }

        public  Task<List<Album>> GetAlbumByUser(int userId)
        {
            var parameters = new NameValueCollection();
            parameters["userId"] = $"{userId}";

            return  GetWhere(parameters);
        }
    }
}

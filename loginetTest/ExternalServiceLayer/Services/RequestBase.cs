﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ExternalServiceLayer.Utilit;
using Newtonsoft.Json;

namespace ExternalServiceLayer.Services
{
    
    public abstract  class RequestBase<T> where T: class , new()
    {
        /// <summary>
        /// Http Client 
        /// </summary>
        readonly HttpClient _client;
        /// <summary>
        /// URI Для запроса
        /// </summary>
        string requestUri;

        protected RequestBase(string requesturi)
        {
            this.requestUri = requesturi;

            _client = new HttpClient { BaseAddress = new Uri(UrlPath.BaseURi) }; 
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


        }

        #region PUBLIC_METHOD        
        /// <summary>
        /// Get All T Element
        /// </summary>
        /// <returns> An Array Of T</returns>
        public async Task<ICollection<T>> GetAll()
        {
            var json = await GetRequest(requestUri);            
            return JsonConvert.DeserializeObject<List<T>>(json);
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <returns>The by identifier.</returns>
        /// <param name="id">Identifier.</param>
        public async Task<T> GetById(int id)
        {
            var requestUri_withParameter = $"{requestUri}?id={id}";
           
            var json = await GetRequest(requestUri_withParameter);
            var result = JsonConvert.DeserializeObject<List<T>>(json)[0];
            return result;
        }

        /// <summary>
        /// Gets the where parameter.
        /// </summary>
        /// <returns>The where.</returns>
        /// <param name="p">P.</param>
        public async Task<List<T>>GetWhere(NameValueCollection p)
        {
            var request_withParameter = requestUri + QueryBuilder.ToQueryString(p);
           
            var json = await GetRequest(request_withParameter);
            return JsonConvert.DeserializeObject<List<T>>(json);
        }
        #endregion

        #region HELPERS
        /// <summary>
        /// Get Request
        /// </summary>
        /// <returns> A Json As String</returns>
        /// <exception cref="Exception"></exception>
        private async Task<String> GetRequest(string withUri)
        {            
            try
            {
                var response = await _client.GetAsync(withUri);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();                   
                }
            }            
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception("Oppps",e);
            }
            return "" ;
        }
        #endregion      
    }  
}
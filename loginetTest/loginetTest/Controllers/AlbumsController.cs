﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ExternalServiceLayer.Interfaces;
using DataLayer.Models.Album;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace loginetTest.Controllers
{
    [Route("api/[controller]")]
    public class AlbumsController : Controller
    {

        readonly IAlbum_Services album_Services; // dependency


        public AlbumsController(IAlbum_Services services)
        {
            album_Services = services;
        }


        /// <summary>
        /// получить список альбомов 
        /// </summary>
        /// <returns>The get.</returns>
        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<Album>> Get()
        {
            return await album_Services.GetAllAlbum();
        }


        /// <summary>
        /// получить альбом по id
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="id">Identifier.</param>
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<Album> Get(int id)
        {
            return await album_Services.GetAlbumByID(id);
        }



        /// <summary>
        /// получить список альбомов по userId
        /// </summary>
        /// <returns>The by user.</returns>
        /// <param name="userId">User identifier.</param>
        [HttpGet("User/{userId}")]
        public async Task<IEnumerable<Album>> AlbumByUser(int userId)
        {
            
            return await album_Services.GetAlbumByUser(userId);
        }

    }
}

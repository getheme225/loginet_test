﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ExternalServiceLayer.Interfaces;
using DataLayer.Models.Users;

namespace loginetTest.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        readonly IUser_Services _userServices; // dependency injection
       
        public UsersController(IUser_Services services)
        {
            _userServices = services;

        }
        /// <summary>
        /// получить список пользователей 
        /// </summary>
        /// <returns>The async.</returns>
        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<User>> GetAsync()
        {

            return await _userServices.GetAllUSers();
        }
        /// <summary>
        /// получить пользователь по id
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="id">Identifier.</param>
        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<User> Get(int id)
        {
            return await _userServices.GetUser(id);
        }

    }
}

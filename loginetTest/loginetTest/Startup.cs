﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ExternalServiceLayer.Interfaces;
using ExternalServiceLayer.Services;

namespace loginetTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("allowed", builder =>
            {
                builder.AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowAnyOrigin();
            }));

            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true; // false by default

            });
            services.AddMvc()
                    .AddXmlSerializerFormatters();
                       
            services.AddScoped<IUser_Services, UserService>(); // must use AddScoped, because it call one's by session 
            services.AddScoped<IAlbum_Services, AlbumService>();
          

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseCors("allowed");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc();
        }   
    }
}

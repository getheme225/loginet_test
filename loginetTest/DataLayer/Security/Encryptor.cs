﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.IO;


namespace DataLayer.Security
{

    public static class Encryptor
    {
        static byte[] KeyGenerator(string key)
        {
            byte[] keyBytes;
            using (SHA256Managed sha = new SHA256Managed())
            {
                keyBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(key));
            }
            return keyBytes;
        }

        #region Encryptor
        public static string Encode(string target, string key)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(target);
            byte[] keyBytes = KeyGenerator(key);

            using (MemoryStream inputStream = new MemoryStream(buffer, false))
            using (MemoryStream outputStream = new MemoryStream())
            using (AesManaged aes = new AesManaged { Key = keyBytes })
            {
                byte[] iv = aes.IV;
                outputStream.Write(iv, 0, iv.Length);
                outputStream.Flush();

                ICryptoTransform encryptor = aes.CreateEncryptor(keyBytes, iv);
                using (CryptoStream crytoStream = new CryptoStream(outputStream, encryptor, CryptoStreamMode.Write))
                {
                    inputStream.CopyTo(crytoStream);
                }

                return Convert.ToBase64String(outputStream.ToArray());
            }
        }
        #endregion
        #region Decryptor

        //public static string Decode(string target, string key)
        //{
        //    byte[] buffer = Convert.FromBase64String(target);
        //    byte[] keyBytes = KeyGenerator(key);
        //    using (MemoryStream inputStream = new MemoryStream(buffer, false))
        //    using (MemoryStream outputStream = new MemoryStream())
        //    using (AesManaged aes = new AesManaged { Key = keyBytes })
        //    {
        //        byte[] iv = new byte[16];
        //        int bytesRead = inputStream.Read(iv, 0, 16);
        //        if (bytesRead < 16)
        //        {
        //            throw new CryptographicException("IV is missing or invalid.");
        //        }

        //        ICryptoTransform decryptor = aes.CreateDecryptor(keyBytes, iv);
        //        using (CryptoStream cryptoStream = new CryptoStream(inputStream, decryptor, CryptoStreamMode.Read))
        //        {
        //            cryptoStream.CopyTo(outputStream);
        //        }

        //        string decryptedValue = Encoding.UTF8.GetString(outputStream.ToArray());
        //        return decryptedValue;
        //    }

        //}
        #endregion
    }
}
﻿using  Newtonsoft.Json;

namespace DataLayer.Models.Users
{
    public class Geolocation
    {
        [JsonProperty("lat")]
        public string Latitude { get; set; }
        
        [JsonProperty("lng")]
        public string Longitude { get; set; }
    }
}
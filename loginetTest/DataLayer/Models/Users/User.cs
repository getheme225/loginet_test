﻿using Newtonsoft.Json;
using DataLayer.Security;

namespace DataLayer.Models.Users
{
    public class User
    {
        private string email = "";
     
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("username")]
        public string Username { get; set; }
        
        [JsonProperty("address")]
        public Address UserAddress { get; set; }

        [JsonProperty("email")]
        public string Email
        {
            get => Encryptor.Encode(email, "12345678"); // Encrypted Email with 
            set => value = email; // from Service 
        }
        
        [JsonProperty("phone")]
        public string Phone { get; set; }
        
        [JsonProperty("website")]
        public string Website { get; set; }
        
        [JsonProperty("company")]
        public Company UserCompany { get; set; }
    }
}

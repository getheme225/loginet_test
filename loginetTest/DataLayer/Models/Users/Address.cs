﻿using  Newtonsoft.Json;
namespace DataLayer.Models.Users
{
    public class Address
    {
        [JsonProperty("street")]
        public string Street { get; set; }  

        [JsonProperty("suite")]
        public string Suite { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }
        
        [JsonProperty("zipCode")]
        public string ZipCode { get; set; }
        
        [JsonProperty("geo")]
        public Geolocation Geo { get; set; }
              
    }
}
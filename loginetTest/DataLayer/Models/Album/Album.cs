﻿using Newtonsoft.Json;

namespace DataLayer.Models.Album
{
    public class Album
    {
        [JsonProperty("userId")]
        public int User_id { get; set; }
        
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}